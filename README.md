# Project Demeter

Assign permissions via policies to roles to access multiple accounts.

Automating via Terraform the information from the [official Nuvens site](https://admin.nuvens.cloud/appendices/multi-aws-accounts).


# Account A: where WSM resides

We need to take notes of the following values in the different accounts. This portion is for the account where WSM appliance is located:
* (existing) AWS Account ID for WSM: `awsaccount_1`
* (existing) AWS Account ID for Remote: `awsaccount_2`
* (existing) IAM Role Name for WSM: `wsm_role`
* (existing) IAM Role ARN for WSM: `wsm_role_arn`
* (existing) IAM Instance Profile ARN for WSM: `wsm_profile_arn`
* (existing) Instance ID for WSM: `wsm_instance`
* (new) IAM Policy Name for WSM: `wsm_policy`

These values must be set on the `variables.tf` file on each respective folder.


# Account B: Secondary connection with an assumed role

In the `remote1` and `remote2` directories, we can configure the account to which WSM will connect with an assumed role via API. We have to configure some variable as well:
* (existing) AWS Account ID for WSM: `awsaccount_1`
* (existing) AWS Account ID for Remote: `awsaccount_2`
* (existing) IAM Role Name for WSM: `wsm_role`
* (existing) IAM Role ARN for WSM: `wsm_role_arn`
* (existing) IAM Instance Profile ARN for WSM: `wsm_profile_arn`
* (new) IAM Role Name for the remote account: `role_b`
* (new) IAM Policy Name for WSM Permissions: `policy_b1`
* (new) IAM Policy Name for WSM IAM Permissions: `policy_b2`
* (new) IAM Policy Name for WSM S3 Permissions: `policy_b3`

1) `remote1` shows an example with very detailed permissions
2) `remote2` shows an example with broader permissions

# Deployment via CloudShell
The deployment mechanism can be executed in different ways. We have tested using GitLab CI/CD Pipelines and GitHub Actions, but since those required an infrastructure to be already in place, like runners; we have added the instructions to be executed directly from the AWS Console via CloudShell.


## Install Terraform in CloudShell
```bash
sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
sudo yum -y install terraform
terraform --version
```


## Clone the Repository
```bash
git clone https://gitlab.com/nuvens-public/multi-accounts-roles-policies.git
```


## Run the code in the AWS Account in which WSM is located
```bash
cd multi-accounts-roles-policies/wsm/
terraform init
terraform validate
terraform plan -out "roles_wsm.tfplan"
terraform apply "roles_wsm.tfplan"
```


## Run the code in the remote AWS Account
```bash
cd multi-accounts-roles-policies/remote/
terraform init
terraform validate
terraform plan -out "roles_remote.tfplan"
terraform apply "roles_remote.tfplan"
```


## Delete (only if needed)
This can be executed after any of the deployments if there has been an error or the right information has not been provided.
```bash
terraform destroy -auto-approve
```
