##################################################################################
# OUTPUTS
##################################################################################

output "aws_iam_policy_name_1" {
  description = "The Name of the IAM Policy 1 for the remote account"
  value       = aws_iam_policy.aws_iam_policy_b1.name
}

output "aws_iam_policy_name_2" {
  description = "The Name of the IAM Policy 2 for the remote account"
  value       = aws_iam_policy.aws_iam_policy_b2.name
}

output "aws_iam_policy_name_3" {
  description = "The Name of the IAM Policy 3 for the remote account"
  value       = aws_iam_policy.aws_iam_policy_b3.name
}

output "aws_iam_role" {
  description = "The Name of the IAM Role for the remote account"
  value       = aws_iam_role.iam_ssm_role_remote.name
}