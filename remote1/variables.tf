##################################################################################
# GLOBAL
##################################################################################

variable "projectname" {
  default = "NUVENS-CLOUD"
}

variable "region" {
  default = "eu-west-1"
}

variable "environment" {
  default = "NON-PRODUCTION"
}

variable "team" {
  default = "DevOps"
}

##################################################################################
# BOOLEAN ANSWER
##################################################################################
variable "yes" {
  default = "true"
}

variable "no" {
  default = "false"
}

##################################################################################
# IAM WSM (ACCOUNT A)
##################################################################################
variable "awsaccount_1" {
  default = "012345678901"
}

variable "wsm_instance" {
  default = "i-99999999999999999"
}

variable "wsm_role" {
  default = "EC2SSM-WSM"
}

variable "wsm_role_arn" {
  default = "arn:aws:iam::012345678901:role/EC2SSM-WSM"
}

variable "wsm_profile_arn" {
  default = "arn:aws:iam::012345678901:instance-profile/EC2SSM-WSM"
}

variable "wsm_policy" {
  default = "WSM_Policy_Test"
}

variable "wsm_policy_1" {
  default = "WSM_Policy_Test"
}

variable "wsm_policy_2" {
  default = "WSMAllowRemoteAccessPolicy"
}

##################################################################################
# IAM REMOTE (ACCOUNT B)
##################################################################################

variable "awsaccount_2" {
  default = "1234567890123"
}

variable "policy_b1" {
  default = "WSMPortalPolicy"
}

variable "policy_b2" {
  default = "WSMPortalIAMPolicy"
}

variable "policy_b3" {
  default = "WSMPortalS3Policy"
}

variable "role_b" {
  default = "AllowWSMAccess"
}
