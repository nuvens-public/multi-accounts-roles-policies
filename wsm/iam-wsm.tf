##################################################################################
# IAM POLICY & ROLE
##################################################################################

resource "aws_iam_policy" "aws_iam_policy_wsm" {
  name = var.wsm_policy
  description = var.wsm_policy

  policy = jsonencode(
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "appstream:*",
        "ce:*",
        "cloudwatch:*",
        "ds:*",
        "kms:*",
        "pricing:*",
        "sts:AssumeRole",
        "workspaces:*"
      ],
      "Resource": [
        "*",
        "arn:aws:iam::${var.awsaccount_2}:role/AllowWSMAccess"
      ],
      "Effect": "Allow",
      "Sid": ""
    }
  ]
})
}

resource "aws_iam_role_policy_attachment" "iam_attach_wsm" {
  depends_on = [ aws_iam_policy.aws_iam_policy_wsm ]
  role       = var.wsm_role
  policy_arn = aws_iam_policy.aws_iam_policy_wsm.arn
}

#resource "aws_iam_policy" "aws_iam_policy_wsm_1" {
#  name = var.wsm_policy_1
#  description = var.wsm_policy_1
#
#  policy = <<EOT
#{
#  "Version": "2012-10-17",
#  "Statement": [
#    {
#      "Action": [
#        "appstream:*",
#        "ce:*",
#        "cloudwatch:*",
#        "ds:*",
#        "kms:*",
#        "pricing:*",
#        "sts:AssumeRole",
#        "workspaces:*"
#      ],
#      "Resource": [
#        "*",
#        "arn:aws:iam::087770160334:role/AllowWSMAccess"
#      ],
#      "Effect": "Allow",
#      "Sid": ""
#    }
#  ]
#}
#EOT
#}
