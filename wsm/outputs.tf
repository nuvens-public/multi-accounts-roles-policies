##################################################################################
# OUTPUTS
##################################################################################

output "aws_iam_policy_name" {
  description = "The Name of the IAM Policy just deployed for WSM"
  value       = aws_iam_policy.aws_iam_policy_wsm.name
}