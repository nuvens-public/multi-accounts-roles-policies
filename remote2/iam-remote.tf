##################################################################################
# IAM POLICY & ROLE
##################################################################################

resource "aws_iam_policy" "aws_iam_policy_b1" {
  name = var.policy_b1
  description = var.policy_b1

  policy = jsonencode(
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "appstream:*",
        "ce:*",
        "cloudwatch:*",
        "ds:*",
        "ec2:Describe*",
        "kms:*",
        "logs:*",
        "pricing:*",
        "sts:GetAccessKeyInfo",
        "sts:GetSessionToken",
        "sts:GetServiceBearerToken",
        "sts:GetCallerIdentity",
        "workspaces:*"
      ],
      "Resource": [
        "*"
      ],
      "Effect": "Allow",
      "Sid": ""
    },
    {
      "Action": [
        "sts:AssumeRole",
        "sts:GetFederationToken"
      ],
      "Resource": [
        "arn:aws:iam::${var.awsaccount_1}:role/${var.wsm_role}",
        "arn:aws:iam::${var.awsaccount_2}:role/${var.role_b}"
      ],
      "Effect": "Allow",
      "Sid": ""
    }
  ]
})
}

resource "aws_iam_policy" "aws_iam_policy_b2" {
  name = var.policy_b2
  description = var.policy_b2

  policy = jsonencode(
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "iam:GetRole",
        "iam:PassRole",
        "sts:AssumeRole"
      ],
      "Resource": [
        "arn:aws:iam::${var.awsaccount_1}:role/${var.wsm_role}",
        "arn:aws:sts::${var.awsaccount_1}:assumed-role/${var.wsm_role}/${var.wsm_instance}"
      ],
      "Effect": "Allow",
      "Sid": ""
    }
  ]
})
}

resource "aws_iam_policy" "aws_iam_policy_b3" {
  name = var.policy_b3
  description = var.policy_b3

  policy = jsonencode(
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:GetBucketVersioning",
        "s3:GetObject",
        "s3:ListAllMyBuckets",
        "s3:ListBucket"
      ],
      "Resource": [
        "*"
      ],
      "Effect": "Allow",
      "Sid": ""
    },
    {
      "Action": [
        "sts:AssumeRole",
        "sts:GetFederationToken"
      ],
      "Resource": [
        "arn:aws:iam::${var.awsaccount_1}:role/${var.wsm_role}",
        "arn:aws:iam::${var.awsaccount_2}:role/AllowWSMAccess"
      ],
      "Effect": "Allow",
      "Sid": ""
    }
  ]
})
}

resource "aws_iam_role" "iam_ssm_role_remote" {
  name = var.role_b

  assume_role_policy = jsonencode(
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Principal": {
        "AWS": [
          "arn:aws:iam::${var.awsaccount_1}:root",
          "arn:aws:iam::${var.awsaccount_1}:role/${var.wsm_role}",
          "arn:aws:sts::${var.awsaccount_1}:assumed-role/${var.wsm_role}/${var.wsm_instance}"
        ],
        "Service": [ "workspaces.amazonaws.com", "ec2.amazonaws.com" ]
      },
      "Action": [
        "sts:AssumeRole"
      ],
      "Effect": "Allow",
      "Sid": ""
    }
  ]
})
}

resource "aws_iam_role_policy_attachment" "iam_attach_allow_1" {
  depends_on = [ aws_iam_policy.aws_iam_policy_b1 ]
  role       = var.role_b
  policy_arn = aws_iam_policy.aws_iam_policy_b1.arn
}

resource "aws_iam_role_policy_attachment" "iam_attach_allow_2" {
  depends_on = [ aws_iam_policy.aws_iam_policy_b2 ]
  role       = var.role_b
  policy_arn = aws_iam_policy.aws_iam_policy_b2.arn
}

resource "aws_iam_role_policy_attachment" "iam_attach_allow_3" {
  depends_on = [ aws_iam_policy.aws_iam_policy_b3 ]
  role       = var.role_b
  policy_arn = aws_iam_policy.aws_iam_policy_b3.arn
}