##################################################################################
# AMI DATA SOURCE
##################################################################################

# Get latest AMI ID for Ubuntu 22.04 OS X86_64
data "aws_ami" "ubuntu" {
  most_recent = true
  owners = [ "099720109477" ]
  filter {
    name = "name"
    values = [ "ubuntu-*22.04*-amd64*" ]
  }
  filter {
    name = "root-device-type"
    values = [ "ebs" ]
  }
  filter {
    name = "virtualization-type"
    values = [ "hvm" ]
  }
  filter {
    name = "architecture"
    values = [ "x86_64" ]
  }
}

##################################################################################
# DATA
##################################################################################

data "aws_availability_zones" "available" {}

data "aws_caller_identity" "current" {}

data "aws_region" "current" {}
